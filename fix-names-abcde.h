#pragma once

#include <spdlog/spdlog.h>
#include <spdlog/fmt/fmt.h>

#include <string>
#include <string_view>

enum TrackType {AlbumWithArtist, AlbumWithoutArtist, CompilationAlbum};

inline std::optional<TrackType> get_type(const std::string& name) {
	std::size_t pos_dot = name.find_first_of(".");
	if (pos_dot == std::string::npos)
		return {};

	std::size_t pos_dash = name.find_first_of("-");
	std::size_t pos_space = name.find_first_of(" - ");
	if (pos_dash == std::string::npos && pos_space == std::string::npos)
		return AlbumWithoutArtist;

	std::size_t pos_dash_space = std::min(pos_dash, pos_space);

	return pos_dot > pos_dash_space ? AlbumWithArtist : CompilationAlbum;
}

inline std::string make_album_without_artist_name(const std::string& name) {
	// 03.This_Is_the_Right_Time.flac
	// track:  [begin -> .)
	// title:  (. -> extension)
	auto extract_track = [&name]() -> std::string_view {
		return {name.c_str(), name.find_first_of(".")};
	};

	auto track{extract_track()};

	return fmt::format("track={}", std::string{track.data(), track.size()});
}

inline std::string make_album_name(const std::string& name) {
	// The\_Jones\_Girls-10-On\_Target_(Reprise).flac
	// artist: [begin -> -)
	// track:  (- -> -)
	// title:  (- -> extension)
	auto extract_artist = [&name]() -> std::string_view {
		return {name.c_str(), name.find_first_of("-")};
	};

	auto artist{extract_artist()};

	return fmt::format("artist={}", std::string{artist.data(), artist.size()});
}

inline std::string make_compilation_name(const std::string& name) {
	// 3/2-12.Jagged_Edge_(2)_&_Nelly-Where_The_Party_At.flac"
	// track:  [begin -> .)
	// artist: (. -> -)
	// track:  (- -> .)
	auto extract_track = [&name]()-> std::string_view {
		return {name.c_str(), name.find_first_of(".")};
	};

	auto track{extract_track()};

	return fmt::format("track={}", std::string{track.data(), track.size()});
}

inline std::string make_name(const std::string& name) {
	auto type{ get_type(name) };
	if (!type)
		throw std::runtime_error{fmt::format("undefined album encoding for \"{}\"", name)};

	switch (*type) {
	case AlbumWithoutArtist:
		return make_album_without_artist_name(name);

	case AlbumWithArtist:
		return make_album_name(name);

	case CompilationAlbum:
		return make_compilation_name(name);
	}
}
