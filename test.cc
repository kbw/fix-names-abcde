#include "fix-names-abcde.h"

#include <gtest/gtest.h>

TEST(TrackTypeTests, AlbumWithArtist) {
	std::string name{"The_Jones_Girls-10-On_Target_(Reprise).flac"};

	auto rc = get_type(name);
	EXPECT_NE(rc, std::nullopt);
}
