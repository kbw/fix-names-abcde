#include "fix-names-abcde.h"

//#include <spdlog/spdlog.h>
//#include <spdlog/fmt/fmt.h>

#include <algorithm>
#include <filesystem>
#include <iostream>
#include <stdexcept>
//#include <string>
//#include <string_view>

#include <string.h>

namespace fs = std::filesystem;

bool repair_filenames_in_dir(fs::path path);

int main(int argc, char* argv[]) {
	std::all_of(argv + 1, argv + argc, repair_filenames_in_dir);
}

//----------------------------------------------------------------------------
/*

enum TrackType {AlbumWithArtist, AlbumWithoutArtist, CompilationAlbum};

std::optional<TrackType> get_type(const std::string& name) {
	std::size_t pos_dot = name.find_first_of(".");
	if (pos_dot == std::string::npos)
		return {};

	std::size_t pos_dash = name.find_first_of("-");
	std::size_t pos_space = name.find_first_of(" - ");
	if (pos_dash == std::string::npos && pos_space == std::string::npos)
		return AlbumWithoutArtist;

	std::size_t pos_dash_space = std::min(pos_dash, pos_space);

	return pos_dot > pos_dash_space ? AlbumWithArtist : CompilationAlbum;
}

std::string make_album_without_artist_name(const std::string& name) {
	// 03.This_Is_the_Right_Time.flac
	// track:  [begin -> .)
	// title:  (. -> extension)
	auto extract_track = [&name]() -> std::string_view {
		return {name.c_str(), name.find_first_of(".")};
	};

	auto track{extract_track()};

	return fmt::format("track={}", std::string{track.data(), track.size()});
}

std::string make_album_name(const std::string& name) {
	// The\_Jones\_Girls-10-On\_Target_(Reprise).flac
	// artist: [begin -> -)
	// track:  (- -> -)
	// title:  (- -> extension)
	auto extract_artist = [&name]() -> std::string_view {
		return {name.c_str(), name.find_first_of("-")};
	};

	auto artist{extract_artist()};

	return fmt::format("artist={}", std::string{artist.data(), artist.size()});
}

std::string make_compilation_name(const std::string& name) {
	// 3/2-12.Jagged_Edge_(2)_&_Nelly-Where_The_Party_At.flac"
	// track:  [begin -> .)
	// artist: (. -> -)
	// track:  (- -> .)
	auto extract_track = [&name]()-> std::string_view {
		return {name.c_str(), name.find_first_of(".")};
	};

	auto track{extract_track()};

	return fmt::format("track={}", std::string{track.data(), track.size()});
}

std::string make_name(const std::string& name) {
	auto type{ get_type(name) };
	if (!type)
		throw std::runtime_error{fmt::format("undefined album encoding for \"{}\"", name)};

	switch (*type) {
	case AlbumWithoutArtist:
		return make_album_without_artist_name(name);

	case AlbumWithArtist:
		return make_album_name(name);

	case CompilationAlbum:
		return make_compilation_name(name);
	}
}
 */

void rename_file(const fs::directory_entry& dirent) {
	try {
//		spdlog::info("file: {}", dirent.path().c_str());
		std::string oldname = dirent.path().filename();
		std::string newname = make_name(oldname);
		spdlog::info("rename: \"{}\" -> \"{}\"", oldname, newname);
	}
	catch (const std::exception& e) {
		spdlog::error("error: {}", e.what());
	}
}

bool repair_filenames_in_dir(fs::path path) {
	try {
		spdlog::info("dir: {}", path.c_str());

		for (const auto& dirent : fs::directory_iterator(path)) {
			if (dirent.is_directory())
				repair_filenames_in_dir(path /= dirent.path());
			else if (dirent.is_regular_file())
				rename_file(dirent);
		}
	}
	catch (const fs::filesystem_error& e) {
		spdlog::error("filesystem_error: {}", e.what());
	}

	return true;
}
