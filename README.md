# NAME:
fix-names-abcde

# DESCRIPTION:
fixup audio cd rip'd filenames as produced by abcde

# RUN:
fix-names-abcde \[directory\] \[directory\] ... 

# DETAILS:
abcde generates output in two formats:
1. Compilation CD with different artists
2. Single artist CD

The CD directory names are fixed up manually as:
1. Various Artists/<album>
2. <artist>/<album>

## Compilation CD File Format:
Example: 1-17.P.\_Diddy,\_Black\_Rob\_&\_Mark\_Curry-Bad\_Boy\_For\_Life.flac

Track: [begin, .)
Artist: (., -)
Title: (-, extension)

## Album CD File Format:
Example: The\_Jones\_Girls-10-On\_Target_(Reprise).flac
Artist: [begin, -)
Track: (-, -)
Title: (-, extension)

Replace _ with <space>

Required output file format:
Track<space>Title.extention

using the examples above:
1-17 Bad Boy For Life.flac
10 On Target.flac
